﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Entrega.Bloc.Entities
{
    public class Responsavel
    {
        public Responsavel(string nome)
        {
            Nome = nome ?? throw new ArgumentNullException(nameof(nome));
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Nome")]
        public string Nome { get; set; }
    }
}
