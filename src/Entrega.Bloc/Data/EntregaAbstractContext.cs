﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace Entrega.Bloc.Data
{
    public abstract class EntregaAbstractContext : IEntregaContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;
        private readonly string _databaseName;
        private readonly string _collectionName;

        public EntregaAbstractContext(string databaseName, string connectionString, string collectionName)
        {
            _collectionName = collectionName ?? throw new ArgumentNullException(nameof(collectionName));
            _databaseName = databaseName ?? throw new ArgumentNullException(nameof(databaseName));
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(databaseName);
            if (CollectionExists())
            {
                Entregas = _database.GetCollection<Entities.Entrega>(collectionName);
            }
            else
            {
                _database.CreateCollection(collectionName);
                Entregas = _database.GetCollection<Entities.Entrega>(collectionName);
            }
            EntregaContextSeed.SeedData(Entregas);
        }

        public IMongoCollection<Entities.Entrega> Entregas { get; }

        private IMongoDatabase GetDatabase() => _client.GetDatabase(_databaseName);
        private bool CollectionExists()
        {
            var filter = new BsonDocument("name", _collectionName);
            var collections = GetDatabase().ListCollectionsAsync(new ListCollectionsOptions { Filter = filter });
            return collections.Result.Any();
        }
        
    }
}
