﻿using Entrega.Bloc.Entities.Enums;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Entrega.Bloc.Data
{
    public class EntregaContextSeed
    {
        public static void SeedData(IMongoCollection<Entities.Entrega> productCollection)
        {
            bool existProduct = productCollection.Find(p => true).Any();
            if (!existProduct)
            {
                productCollection.InsertManyAsync(ObterEntregas());
            }
        }

        private static IEnumerable<Entities.Entrega> ObterEntregas()
        {
            DateTime agora = DateTime.Now;
            int numero = 5;
            return new List<Entities.Entrega>()
            {
                new Entities.Entrega
                {
                    Id = "6232a63d621462f9da9fd6de",
                    Status = StatusEntrega.Andamento,
                    Carga = "Celulares Moto G20",
                    Destinatario = "Casa Grande. CNPJ 64.767.749/0001-41",
                    Cliente = "Mercadão dos celulares. CNPJ 57.058.338/0001-11",
                    Rota = "Pontos: A, B, C, D",
                    PrazoEmDiasUteis = numero,
                    Transporte =
                    new Entities.Transporte {
                        Id = "6233eaf9dc2b6fa11c5576a3",
                        Nome = "Caminhao I",
                        Latitude = new decimal(-1.4),
                        Longitude = new decimal(-27.13)
                    },
                    Inicio = agora.AddDays(-numero),
                    Fim = agora,
                    Responsavel = new Entities.Responsavel("Kleber")
                    {
                        Id = "622639ee50baa69274b1d2fa"
                    }
                }
            };
        }
    }
}
