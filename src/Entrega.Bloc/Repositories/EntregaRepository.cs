﻿using Entrega.Bloc.Data;
using Entrega.Bloc.Entities.Enums;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Entrega.Bloc.Repositories
{
    public class EntregaRepository : IEntregaRepository
    {
        private readonly IEntregaContext _context;

        public EntregaRepository(IEntregaContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task Atualizar(Entities.Entrega entrega)
        {

            await _context.Entregas.ReplaceOneAsync(
                filter: g => g.Id == entrega.Id, replacement: entrega);
        }

        public async Task Criar(Entities.Entrega entrega)
        {
            await _context.Entregas.InsertOneAsync(entrega);
        }

        public async Task<Entities.Entrega> Obter(string id)
        {
            var result = await _context.Entregas.FindAsync<Entities.Entrega>(filter: p => p.Id.Equals(id));

            return await result.FirstAsync();
        }

        public async Task<Entities.Entrega> ObterPorTransporteId(string transporteId)
        {
            var result = await _context.Entregas.FindAsync(filter: e => e.Transporte.Id.Equals(transporteId) && e.Status == StatusEntrega.Andamento);
            return await result.FirstAsync();
        }
    }
}
