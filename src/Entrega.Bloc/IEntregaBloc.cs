﻿using Entrega.Bloc.Entities;
using System.Threading.Tasks;

namespace Entrega.Bloc
{
    public interface IEntregaBloc
    {
        public Task AtualizacaoLocalizacaoTransporte(Transporte transporte);
        public Task Criar(Entities.Entrega entrega);
        public Task FinalizarEntrega(string id);
        public Task<Entities.Entrega> Obter(string id);
    }
}
