﻿using Microsoft.AspNetCore.Mvc;
using Responsavel.Bloc;
using System;
using System.Threading.Tasks;

namespace Responsavel.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ResponsavelController : ControllerBase
    {
        private readonly IResponsavelBloc _bloc;

        public ResponsavelController(IResponsavelBloc bloc)
        {
            _bloc = bloc ?? throw new ArgumentNullException(nameof(bloc));
        }

        [HttpGet("{id}", Name = "Obter")]
        public async Task<ActionResult<Bloc.Entities.Responsavel>> Obter(string id)
        {
            var responsavel = await _bloc.Obter(id);
            return Ok(responsavel);
        }

        [HttpPut("{idResponsavel}/entrega/{idEntrega}/encerramento")]
        public async Task<ActionResult> FinalizarEntrega(string idResponsavel, string idEntrega)
        {
            await _bloc.FinalizarEntrega(idResponsavel, idEntrega);
            return Ok();
        }
    }
}
