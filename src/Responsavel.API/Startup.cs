using BoaEntrega.Commons;
using BoaEntrega.Commons.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Responsavel.API.Context;
using Responsavel.API.Converter;
using Responsavel.API.Message;
using Responsavel.Bloc;
using Responsavel.Bloc.Data;
using Responsavel.Bloc.Repositories;

namespace Responsavel.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IResponsavelBloc, ResponsavelBloc>();
            services.AddScoped<IResponsavelRepository, ResponsavelRepository>();
            services.AddScoped<IResponsavelContext, ResponsavelContext>();
            services.AddScoped<IMessageSender, EntregaFinalizadaKafkaSender>();
            services.AddScoped<IJsonConverter<Bloc.Entities.Responsavel>, ResponsavelJsonConverter>();


            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Responsavel.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Responsavel.API v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
