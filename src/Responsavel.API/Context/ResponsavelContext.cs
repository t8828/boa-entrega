﻿using Microsoft.Extensions.Configuration;
using Responsavel.Bloc.Data;

namespace Responsavel.API.Context
{
    public class ResponsavelContext : ResponsavelAbstractContext
    {
        public ResponsavelContext(IConfiguration configuration) :
            base(
                configuration.GetValue<string>("DatabaseSettings:DatabaseName"),
                configuration.GetValue<string>("DatabaseSettings:ConnectionString"),
                configuration.GetValue<string>("DatabaseSettings:CollectionName"))
        {
        }
    }
}
