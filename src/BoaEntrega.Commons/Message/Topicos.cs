﻿namespace BoaEntrega.Commons.Message
{
    public enum Topicos
    {
        EntregasFinalizadas,
        EntregasCriadas,
        LocalizacaoTransporte
    }
}
