﻿using System.Threading.Tasks;

namespace BoaEntrega.Commons
{
    public interface IMessageSender
    {
        public Task Enviar(string mensagem);
    }
}
