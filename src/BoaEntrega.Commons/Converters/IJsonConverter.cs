﻿namespace BoaEntrega.Commons.Converters
{
    public interface IJsonConverter<T>
    {
        public T Deserialize(string json);
        public string Serialize(T obj);
    }
}
