﻿using BoaEntrega.Commons.Converters;
using BoaEntrega.Commons.Message;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Responsavel.Bloc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Responsavel.Bloc.Converter;
using Responsavel.Bloc.ViewModels;

namespace Responsavel.Consumer.Message
{
    public class EntregaCriadaKafkaMessageHandler : IMessageHandler<EntregaViewModel>
    {
        private readonly ILogger _logger;
        private readonly string _bootstrapServers;
        private readonly string _topic;
        private readonly string _groupId;
        private readonly IResponsavelBloc _bloc;
        private readonly IJsonConverter<EntregaViewModel> _jsonConverter;
        public EntregaCriadaKafkaMessageHandler(IConfiguration configuration, ILogger<EntregaCriadaKafkaMessageHandler> logger, IResponsavelBloc bloc, IJsonConverter<Bloc.ViewModels.EntregaViewModel> jsonConverter)
        {
            _logger = logger;
            _bootstrapServers = configuration.GetValue<string>("KafkaSettings:BootstrapServers");
            _topic = Topicos.EntregasCriadas.ToString();
            _groupId = configuration.GetValue<string>("KafkaSettings:GroupId");
            _bloc = bloc;
            _jsonConverter = jsonConverter;
        }
        public async Task MessageHandler()
        {
            var conf = new ConsumerConfig
            {
                BootstrapServers = _bootstrapServers,
                GroupId = _groupId
            };

            using (var c = new ConsumerBuilder<Ignore, string>(conf).Build())
            {
                PrintMetadata();
                if (!ExisteTopico())
                    await CreateTopicAsync();
                c.Subscribe(_topic);
                var cts = new CancellationTokenSource();

                try
                {
                    while (true)
                    {
                        var message = c.Consume(cts.Token).Message;
                        try
                        {
                            Bloc.ViewModels.EntregaViewModel entregaViewModel = _jsonConverter.Deserialize(message.Value);
                            

                            await _bloc.AdicionarEntrega(entregaViewModel);
                        } catch (Exception ex)
                        {
                            _logger.LogError(ex.InnerException.Message);
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    c.Close();
                }
            }
        }
        static string ToString(int[] array) => $"[{string.Join(", ", array)}]";
        private void PrintMetadata()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                // Warning: The API for this functionality is subject to change.
                var meta = adminClient.GetMetadata(TimeSpan.FromSeconds(20));
                _logger.LogInformation($"{meta.OriginatingBrokerId} {meta.OriginatingBrokerName}");
                meta.Brokers.ForEach(broker =>
                    _logger.LogInformation($"Broker: {broker.BrokerId} {broker.Host}:{broker.Port}"));

                meta.Topics.ForEach(topic =>
                {
                    _logger.LogInformation($"Topic: {topic.Topic} {topic.Error}");
                    topic.Partitions.ForEach(partition =>
                    {
                        _logger.LogInformation($"  Partition: {partition.PartitionId}");
                        _logger.LogInformation($"    Replicas: {ToString(partition.Replicas)}");
                        _logger.LogInformation($"    InSyncReplicas: {ToString(partition.InSyncReplicas)}");
                    });
                });
            }
        }

        private bool ExisteTopico()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                var meta = adminClient.GetMetadata(TimeSpan.FromSeconds(20));
                return meta.Topics.Exists(topic => topic.Topic.Equals(_topic));
            }
        }

        private async Task CreateTopicAsync()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                try
                {
                    await adminClient.CreateTopicsAsync(new TopicSpecification[] {
                        new TopicSpecification { Name = _topic, ReplicationFactor = 1, NumPartitions = 1 } });
                }
                catch (CreateTopicsException e)
                {
                    _logger.LogError($"An error occured creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
                }
            }
        }
    }
}
