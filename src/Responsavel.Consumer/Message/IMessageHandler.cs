﻿using System.Threading.Tasks;

namespace Responsavel.Consumer.Message
{
    public interface IMessageHandler<T>
    {
        Task MessageHandler();
    }
}
