﻿using BoaEntrega.Commons.Converters;
using System.Text.Json;

namespace Responsavel.WorkerService.Converters
{
    public class EntregaViewModelJsonConverter : IJsonConverter<Bloc.ViewModels.EntregaViewModel>
    {
        public Bloc.ViewModels.EntregaViewModel Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Bloc.ViewModels.EntregaViewModel>(json);
        }

        public string Serialize(Bloc.ViewModels.EntregaViewModel obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
