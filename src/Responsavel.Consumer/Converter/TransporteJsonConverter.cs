﻿using BoaEntrega.Commons.Converters;
using Responsavel.Bloc.Entities;
using System.Text.Json;

namespace Responsavel.WorkerService.Converters
{
    public class TransporteJsonConverter : IJsonConverter<Transporte>
    {
        public Transporte Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Transporte>(json);
        }

        public string Serialize(Transporte obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
