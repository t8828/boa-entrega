using BoaEntrega.Commons.Converters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Responsavel.Bloc;
using Responsavel.Bloc.Data;
using Responsavel.Bloc.Entities;
using Responsavel.Bloc.Repositories;
using Responsavel.Bloc.ViewModels;
using Responsavel.Consumer.Context;
using Responsavel.Consumer.Message;
using Responsavel.WorkerService.Converters;

namespace Responsavel.Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddScoped<IMessageHandler<EntregaViewModel>, EntregaCriadaKafkaMessageHandler>();
                    services.AddScoped<IMessageHandler<Transporte>, LocalizacaoAtualizadaKafkaMessageHandler>();
                    services.AddScoped<IResponsavelBloc, ResponsavelBloc>();
                    services.AddScoped<IResponsavelRepository, ResponsavelRepository>();
                    services.AddScoped<IResponsavelContext, ResponsavelContext>();
                    services.AddScoped<IJsonConverter<Bloc.ViewModels.EntregaViewModel>, EntregaViewModelJsonConverter>();
                    services.AddScoped<IJsonConverter<Transporte>, TransporteJsonConverter>();
                });
    }
}
