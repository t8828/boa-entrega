﻿using Microsoft.Extensions.Configuration;
using Entrega.Bloc.Data;

namespace Entrega.API.Context
{
    public class EntregaContext : EntregaAbstractContext
    {
        public EntregaContext(IConfiguration configuration) :
            base(
                configuration.GetValue<string>("DatabaseSettings:DatabaseName"),
                configuration.GetValue<string>("DatabaseSettings:ConnectionString"),
                configuration.GetValue<string>("DatabaseSettings:CollectionName"))
        {
        }
    }
}
