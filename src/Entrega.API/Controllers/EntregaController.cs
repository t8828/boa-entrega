﻿using Microsoft.AspNetCore.Mvc;
using Entrega.Bloc;
using System;
using System.Threading.Tasks;

namespace Entrega.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EntregaController : ControllerBase
    {
        private readonly IEntregaBloc _bloc;

        public EntregaController(IEntregaBloc bloc)
        {
            _bloc = bloc ?? throw new ArgumentNullException(nameof(bloc));
        }

        [HttpGet("{id}", Name = "Obter")]
        public async Task<ActionResult<Bloc.Entities.Entrega>> Obter(string id)
        {
            var entrega = await _bloc.Obter(id);
            return Ok(entrega);
        }

        [HttpPost()]
        public async Task<ActionResult<Bloc.Entities.Entrega>> Criar([FromBody] Bloc.Entities.Entrega entrega)
        {
            await _bloc.Criar(entrega);
            return CreatedAtRoute("Obter", new { id = entrega.Id }, entrega);
        }


    }
}
