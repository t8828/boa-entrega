using BoaEntrega.Commons;
using BoaEntrega.Commons.Converters;
using Entrega.API.Context;
using Entrega.API.Converter;
using Entrega.API.Message;
using Entrega.Bloc;
using Entrega.Bloc.Data;
using Entrega.Bloc.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Entrega.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IEntregaBloc, EntregaBloc>();
            services.AddScoped<IEntregaRepository, EntregaRepository>();
            services.AddScoped<IMessageSender, EntregaCriadaKafkaSender>();
            services.AddScoped<IJsonConverter<Bloc.Entities.Entrega>, EntregaJsonConverter>();
            services.AddScoped<IEntregaContext, EntregaContext>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Entrega.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Entrega.API v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
