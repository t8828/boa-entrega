﻿using BoaEntrega.Commons.Converters;
using System.Text.Json;

namespace Entrega.API.Converter
{
    public class EntregaJsonConverter : IJsonConverter<Bloc.Entities.Entrega>
    {
        public Bloc.Entities.Entrega Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Bloc.Entities.Entrega>(json);
        }

        public string Serialize(Bloc.Entities.Entrega obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
