﻿using BoaEntrega.Commons.Converters;
using System.Text.Json;

namespace Transporte.API.Converter
{
    public class TransporteJsonConverter : IJsonConverter<Bloc.Entities.Transporte>
    {
        public Bloc.Entities.Transporte Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Bloc.Entities.Transporte>(json);
        }

        public string Serialize(Bloc.Entities.Transporte obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
