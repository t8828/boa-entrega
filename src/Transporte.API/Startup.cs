using BoaEntrega.Commons;
using BoaEntrega.Commons.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Responsavel.API.Message;
using Transporte.API.Context;
using Transporte.API.Converter;
using Transporte.Bloc;
using Transporte.Bloc.Data;
using Transporte.Bloc.Repositories;

namespace Transporte.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ITransporteBloc, TransporteBloc>();
            services.AddScoped<ITransporteRepository, TransporteRepository>();
            services.AddScoped<ITransporteContext, TransporteContext>();
            services.AddScoped<IMessageSender, LocalizacaoAtualizadaKafkaSender>();
            services.AddScoped<IJsonConverter<Bloc.Entities.Transporte>, TransporteJsonConverter>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Transporte.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Transporte.API v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
