﻿using Microsoft.AspNetCore.Mvc;
using Transporte.Bloc;
using System;
using System.Threading.Tasks;

namespace Transporte.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TransporteController : ControllerBase
    {
        private readonly ITransporteBloc _bloc;

        public TransporteController(ITransporteBloc bloc)
        {
            _bloc = bloc ?? throw new ArgumentNullException(nameof(bloc));
        }

        [HttpGet("{id}", Name = "Obter")]
        public async Task<ActionResult<Bloc.Entities.Transporte>> Obter(string id)
        {
            var responsavel = await _bloc.Obter(id);
            return Ok(responsavel);
        }

        [HttpPut("{id}/localizacao/{latitude}/{longitude}")]
        public async Task<ActionResult> AtualizarLocalizacao(string id, decimal latitude, decimal longitude)
        {
            await _bloc.AtualizarLocalizacao(id, latitude, longitude);
            return Ok();
        }
    }
}
