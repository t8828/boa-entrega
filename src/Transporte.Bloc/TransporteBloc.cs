﻿using BoaEntrega.Commons;
using BoaEntrega.Commons.Converters;
using System;
using System.Threading.Tasks;
using Transporte.Bloc.Repositories;

namespace Transporte.Bloc
{
    public class TransporteBloc : ITransporteBloc
    {
        private readonly ITransporteRepository _repository;
        private readonly IMessageSender _sender;
        private readonly IJsonConverter<Entities.Transporte> _json;

        public TransporteBloc(ITransporteRepository repository, IJsonConverter<Entities.Transporte> json, IMessageSender sender)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _sender = sender ?? throw new ArgumentNullException(nameof(sender));
            _json = json ?? throw new ArgumentNullException(nameof(json));
        }

        public async Task AtualizarLocalizacao(string id, decimal latitude, decimal longitude)
        {
            Entities.Transporte transporte = await _repository.Obter(id);
            transporte.Longitude = longitude;
            transporte.Latitude = latitude;
            await _repository.Atualizar(transporte);
            string transporteJson = _json.Serialize(transporte);
            await _sender.Enviar(transporteJson);
        }

        public async Task<Entities.Transporte> Obter(string id)
        {
            return await _repository.Obter(id);
        }
    }
}
