﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Transporte.Bloc.Entities
{
    public class Transporte
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Nome")]
        public string Nome { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
