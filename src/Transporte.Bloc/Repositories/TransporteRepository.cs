﻿using MongoDB.Driver;
using Transporte.Bloc.Data;
using System;
using System.Threading.Tasks;

namespace Transporte.Bloc.Repositories
{
    public class TransporteRepository : ITransporteRepository
    {
        private readonly ITransporteContext _context;

        public TransporteRepository(ITransporteContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task Atualizar(Entities.Transporte transporte)
        {

            await _context.Transportes.ReplaceOneAsync(
                filter: g => g.Id == transporte.Id, replacement: transporte);
        }

        public async Task<Entities.Transporte> Obter(string id)
        {
            var result = await _context.Transportes.FindAsync<Entities.Transporte>(filter: p => p.Id.Equals(id));

            return await result.FirstAsync();
        }
    }
}
