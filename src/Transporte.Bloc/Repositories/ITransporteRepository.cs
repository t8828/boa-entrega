﻿using System.Threading.Tasks;

namespace Transporte.Bloc.Repositories
{
    public interface ITransporteRepository
    {
        Task Atualizar(Entities.Transporte transporte);
        Task<Entities.Transporte> Obter(string id);
    }
}
