﻿using System.Threading.Tasks;

namespace Transporte.Bloc
{
    public interface ITransporteBloc
    {
        public Task<Entities.Transporte> Obter(string id);
        public Task AtualizarLocalizacao(string id, decimal latitude, decimal longitude);
    }
}
