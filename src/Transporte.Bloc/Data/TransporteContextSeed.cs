﻿using MongoDB.Driver;
using System.Collections.Generic;

namespace Transporte.Bloc.Data
{
    public class TransporteContextSeed
    {
        public static void SeedData(IMongoCollection<Entities.Transporte> productCollection)
        {
            bool existProduct = productCollection.Find(p => true).Any();
            if (!existProduct)
            {
                productCollection.InsertManyAsync(ObterTransportes());
            }
        }

        private static IEnumerable<Entities.Transporte> ObterTransportes()
        {
            return new List<Entities.Transporte>()
            {
                new Entities.Transporte {
                    Id = "6233eaf9dc2b6fa11c5576a3",
                    Nome = "Caminhao I",
                    Latitude = new decimal(-1.4),
                    Longitude = new decimal(-27.13)
                },
                new Entities.Transporte {
                    Id = "623fc48002a5e6eb6ef7dd9e",
                    Nome = "Caminhao II",
                    Latitude = new decimal(-1.31231),
                    Longitude = new decimal(-21.45545)
                }
            }; 
        }
    }
}
