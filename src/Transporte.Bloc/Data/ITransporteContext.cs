﻿using MongoDB.Driver;

namespace Transporte.Bloc.Data
{
    public interface ITransporteContext
    {
        IMongoCollection<Entities.Transporte> Transportes { get; }
    }
}
