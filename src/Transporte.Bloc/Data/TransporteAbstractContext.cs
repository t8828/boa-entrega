﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace Transporte.Bloc.Data
{
    public abstract class TransporteAbstractContext : ITransporteContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;
        private readonly string _databaseName;
        private readonly string _collectionName;

        public TransporteAbstractContext(string databaseName, string connectionString, string collectionName)
        {
            _collectionName = collectionName ?? throw new ArgumentNullException(nameof(collectionName));
            _databaseName = databaseName ?? throw new ArgumentNullException(nameof(databaseName));
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(databaseName);
            if (CollectionExists())
            {
                Transportes = _database.GetCollection<Entities.Transporte>(collectionName);
            }
            else
            {
                _database.CreateCollection(collectionName);
                Transportes = _database.GetCollection<Entities.Transporte>(collectionName);
            }
            TransporteContextSeed.SeedData(Transportes);
        }

        public IMongoCollection<Entities.Transporte> Transportes { get; }

        private IMongoDatabase GetDatabase() => _client.GetDatabase(_databaseName);
        private bool CollectionExists()
        {
            var filter = new BsonDocument("name", _collectionName);
            var collections = GetDatabase().ListCollectionsAsync(new ListCollectionsOptions { Filter = filter });
            return collections.Result.Any();
        }
        
    }
}
