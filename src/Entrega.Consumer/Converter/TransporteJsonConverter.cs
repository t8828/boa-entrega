﻿using BoaEntrega.Commons.Converters;
using Entrega.Bloc.Entities;
using System.Text.Json;

namespace Entrega.WorkerService.Converters
{
    public class TransporteJsonConverter : IJsonConverter<Transporte>
    {
        public Transporte Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Transporte>(json);
        }

        public string Serialize(Transporte obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
