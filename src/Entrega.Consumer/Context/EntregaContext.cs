﻿using Entrega.Bloc.Data;
using Microsoft.Extensions.Configuration;

namespace Entrega.Consumer.Context
{
    public class EntregaContext : EntregaAbstractContext
    {
        public EntregaContext(IConfiguration configuration) : 
            base(
                configuration.GetValue<string>("DatabaseSettings:DatabaseName"), 
                configuration.GetValue<string>("DatabaseSettings:ConnectionString"),
                configuration.GetValue<string>("DatabaseSettings:CollectionName"))
        {
        }
    }
}
