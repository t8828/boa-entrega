﻿using BoaEntrega.Commons.Converters;
using BoaEntrega.Commons.Message;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Entrega.Bloc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Entrega.Bloc.Entities;

namespace Entrega.Consumer.Message
{
    public class LocalizacaoAtualizadaKafkaMessageHandler : IMessageHandler<Transporte>
    {
        private readonly ILogger _logger;
        private readonly string _bootstrapServers;
        private readonly string _topic;
        private readonly string _groupId;
        private readonly IEntregaBloc _bloc;
        private readonly IJsonConverter<Transporte> _jsonConverter;
        public LocalizacaoAtualizadaKafkaMessageHandler(IConfiguration configuration, ILogger<LocalizacaoAtualizadaKafkaMessageHandler> logger, IEntregaBloc bloc, IJsonConverter<Transporte> jsonConverter)
        {
            _logger = logger;
            _bootstrapServers = configuration.GetValue<string>("KafkaSettings:BootstrapServers");
            _topic = Topicos.LocalizacaoTransporte.ToString();
            _groupId = configuration.GetValue<string>("KafkaSettings:GroupId");
            _bloc = bloc;
            _jsonConverter = jsonConverter;
        }
        public async Task MessageHandler()
        {
            var conf = new ConsumerConfig
            {
                BootstrapServers = _bootstrapServers,
                GroupId = _groupId
            };

            using (var c = new ConsumerBuilder<Ignore, string>(conf).Build())
            {
                PrintMetadata();
                if (!ExisteTopico())
                    //await CreateTopicAsync();
                c.Subscribe(_topic);
                var cts = new CancellationTokenSource();

                try
                {
                    while (true)
                    {
                        var message = c.Consume(cts.Token).Message;
                        try
                        {
                            var transporte = _jsonConverter.Deserialize(message.Value);
                            await _bloc.AtualizacaoLocalizacaoTransporte(transporte);
                        } catch (Exception ex)
                        {
                            _logger.LogError(ex.InnerException.Message);
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    c.Close();
                }
            }
        }
        static string ToString(int[] array) => $"[{string.Join(", ", array)}]";
        private void PrintMetadata()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                // Warning: The API for this functionality is subject to change.
                var meta = adminClient.GetMetadata(TimeSpan.FromSeconds(20));
                _logger.LogInformation($"{meta.OriginatingBrokerId} {meta.OriginatingBrokerName}");
                meta.Brokers.ForEach(broker =>
                    _logger.LogInformation($"Broker: {broker.BrokerId} {broker.Host}:{broker.Port}"));

                meta.Topics.ForEach(topic =>
                {
                    _logger.LogInformation($"Topic: {topic.Topic} {topic.Error}");
                    topic.Partitions.ForEach(partition =>
                    {
                        _logger.LogInformation($"  Partition: {partition.PartitionId}");
                        _logger.LogInformation($"    Replicas: {ToString(partition.Replicas)}");
                        _logger.LogInformation($"    InSyncReplicas: {ToString(partition.InSyncReplicas)}");
                    });
                });
            }
        }

        private bool ExisteTopico()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                var meta = adminClient.GetMetadata(TimeSpan.FromSeconds(20));
                var existe = meta.Topics.Exists(topic => topic.Topic.Equals(_topic));
                _logger.LogInformation($"meta: {meta}");
                _logger.LogInformation($"meta.Topics: {meta.Topics}");
                _logger.LogInformation("Tópicos existentes");
                meta.Topics.ForEach(t => _logger.LogInformation($"{t.Topic}"));
                _logger.LogInformation($"Existe tópico {_topic}? {existe} às {DateTime.Now}");
                return existe;
            }
        }

        private async Task CreateTopicAsync()
        {
            using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = _bootstrapServers }).Build())
            {
                try
                {
                    await adminClient.CreateTopicsAsync(new TopicSpecification[] {
                        new TopicSpecification { Name = _topic, ReplicationFactor = 1, NumPartitions = 1 } });
                }
                catch (CreateTopicsException e)
                {
                    _logger.LogError($"An error occured creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
                }
            }
        }
    }
}
