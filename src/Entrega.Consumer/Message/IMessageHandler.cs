﻿using System.Threading.Tasks;

namespace Entrega.Consumer.Message
{
    public interface IMessageHandler<T>
    {
        Task MessageHandler();
    }
}
