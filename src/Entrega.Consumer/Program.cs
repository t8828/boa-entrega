using BoaEntrega.Commons.Converters;
using Entrega.Bloc;
using Entrega.Bloc.Data;
using Entrega.Bloc.Entities;
using Entrega.Bloc.Repositories;
using Entrega.Consumer.Context;
using Entrega.Consumer.Message;
using Entrega.WorkerService.Converters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entrega.Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddScoped<IMessageHandler<string>, EntregaFinalizadaKafkaMessageHandler>();
                    services.AddScoped<IMessageHandler<Transporte>, LocalizacaoAtualizadaKafkaMessageHandler>();
                    services.AddScoped<IEntregaBloc, EntregaBloc>();
                    services.AddScoped<IEntregaRepository, EntregaRepository>();
                    services.AddScoped<IEntregaContext, EntregaContext>();
                    services.AddScoped<IJsonConverter<Bloc.Entities.Entrega>, EntregaJsonConverter>();
                    services.AddScoped<IJsonConverter<Transporte>, TransporteJsonConverter>();
                });
    }
}
