﻿using Responsavel.Bloc.Entities;
using Responsavel.Bloc.ViewModels;
using System.Threading.Tasks;

namespace Responsavel.Bloc
{
    public interface IResponsavelBloc
    {
        public Task AtualizacaoLocalizacaoTransporte(Transporte transporte);
        public Task AdicionarEntrega(EntregaViewModel entregaViewModel);
        public Task FinalizarEntrega(string idResponsavel, string idEntrega);
        public Task<Entities.Responsavel> Obter(string idResponsavel);
    }
}
