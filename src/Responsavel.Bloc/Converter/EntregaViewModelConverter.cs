﻿using Responsavel.Bloc.Entities;
using Responsavel.Bloc.ViewModels;

namespace Responsavel.Bloc.Converter
{
    public static class EntregaViewModelConverter
    {
        public static Entrega ToEntrega(EntregaViewModel viewModel)
        {
            return new Entrega
            {
                Id = viewModel.Id,
                Carga = viewModel.Carga,
                Cliente = viewModel.Cliente,
                DataEntrega = viewModel.DataEntrega,
                Destinatario = viewModel.Destinatario,
                Fim = viewModel.Fim,
                Inicio = viewModel.Inicio,
                PrazoEmDiasUteis = viewModel.PrazoEmDiasUteis, 
                Rota = viewModel.Rota,
                Status = viewModel.Status,
                Transporte = viewModel.Transporte,
            };
        }
    }
}
