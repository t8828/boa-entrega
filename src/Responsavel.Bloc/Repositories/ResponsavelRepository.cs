﻿using MongoDB.Driver;
using Responsavel.Bloc.Data;
using Responsavel.Bloc.Entities.Enums;
using System;
using System.Threading.Tasks;

namespace Responsavel.Bloc.Repositories
{
    public class ResponsavelRepository : IResponsavelRepository
    {
        private readonly IResponsavelContext _context;

        public ResponsavelRepository(IResponsavelContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task Atualizar(Entities.Responsavel responsavel)
        {

            await _context.Responsaveis.ReplaceOneAsync(
                filter: g => g.Id == responsavel.Id, replacement: responsavel);
        }

        public async Task<Entities.Responsavel> Obter(string id)
        {
            var result = await _context.Responsaveis.FindAsync<Entities.Responsavel>(filter: p => p.Id.Equals(id));

            return await result.FirstAsync();
        }

        public async Task<Entities.Responsavel> ObterPorTransporteId(string transporteId)
        {
            var todos = await _context.Responsaveis.FindAsync(filter: r => true);
            var responsaveis = await todos.ToListAsync(); 
            return responsaveis.Find(r => {
                if (r.Entregas != null)
                    return r.Entregas.Exists(e => e.Transporte.Id.Equals(transporteId) && e.Status == StatusEntrega.Andamento);
                return false;
            });
        }
        
    } 
}
