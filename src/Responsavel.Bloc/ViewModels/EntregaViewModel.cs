﻿using Responsavel.Bloc.Entities;
using Responsavel.Bloc.Entities.Enums;
using System;
using System.Text.Json.Serialization;

namespace Responsavel.Bloc.ViewModels
{
    public class EntregaViewModel
    {
        public string Id { get; set; }
        public string Carga { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public StatusEntrega Status { get; set; }
        public string Destinatario { get; set; }
        public string Cliente { get; set; }
        public string Rota { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int PrazoEmDiasUteis { get; set; }
        public DateTime DataEntrega { get; set; }
        public Transporte Transporte { get; set; }
        public ResponsavelViewModel Responsavel { get; set; }
    }
}
