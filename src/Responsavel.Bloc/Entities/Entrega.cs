﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Responsavel.Bloc.Entities.Enums;
using System;
using System.Text.Json.Serialization;

namespace Responsavel.Bloc.Entities
{
    public class Entrega
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Carga")]
        public string Carga { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public StatusEntrega Status { get; set; }
        public string Destinatario { get; set; }
        public string Cliente { get; set; }
        public string Rota { get; set; }
        public DateTime Inicio  { get; set; }
        public DateTime Fim { get; set; }
        public int PrazoEmDiasUteis { get; set; }
        public DateTime DataEntrega { get; set; }
        public Transporte Transporte { get; set; }
    }
}
