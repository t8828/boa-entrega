﻿using MongoDB.Driver;

namespace Responsavel.Bloc.Data
{
    public interface IResponsavelContext
    {
        IMongoCollection<Entities.Responsavel> Responsaveis { get; }
    }
}
