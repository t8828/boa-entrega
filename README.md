# Boa Entrega

Prova de conceito arquitetural de uma solução de logística. A arquitetura é baseada na abordagem de microsserviços.

## BPMN
![](doc/arquitetura-bpmn.png)

## Arquitetura
![](doc/arquitetura-desenho.png)

## Componentes

* BoaEntrega.APIGw (container): API Gateway desenvolvida com Ocelot.

* Responsavel.API (container): API REST desenvolvida com Dot NET 5.
* responsaveldb (container): Banco de dados MongoDB

* Entrega.API (container): API REST desenvolvida com Dot NET 5.
* entregadb (container): Banco de dados MongoDB
* Entrega.Consumer (container): Consumidor de mensagens do Kafka. Desenvolvida com Dot Net 5.

* Broker (container): Mensageria com Kafka.

* Entrega.Bloc: Lógica de negócio da gestão de entrega. Biblioteca (Class Library) desenvolvida com Dot Net 5. 

* Responsavel.Bloc: Lógica de negócio da gestão de responsáveis. Biblioteca (Class Library) desenvolvida com Dot Net 5.

* BoaEntrega.Commons: Funcionalidade comuns a qualquer componente. Biblioteca (Class Library) desenvolvida com Dot Net 5.

## Prova de Conceito (POC) e Protótipo Arquitetural 
Esta seção apresenta os detalhes da POC do Protótipo Arquitetural da Boa Entrega. O código-fonte dessa POC está em https://gitlab.com/t8828/boa-entrega.git

### Implementação
Os casos de uso selecionados para implementação da POC são: (i) Criar entrega; (ii) Finalizar entrega; e (iii) Atualizar localização.

As imagens logo abaixo apresentam os protótipos de telas dos casos de uso mencionados. O caso de uso (iii) é executado por sistema de rastreamento contratado pela Boa Entrega para monitorar sua frota de veículos.

![](doc/prototipos-de-telas/entrega-criar.png)
![](doc/prototipos-de-telas/entrega-localizacao.png)
![](doc/prototipos-de-telas/responsavel-status.png)
![](doc/prototipos-de-telas/responsavel-localizacao-transporte.png)

A implementação dos três casos de uso aborda todos os aspectos arquiteturais definidos neste trabalho. Por isso, pode-se escolher qualquer um para detalhar seus componentes a fim de entendimentos do protótipo arquitetural.

O caso de uso (i) permite criar entregas a partir de um frontend web que faz a gestão. Após criar uma entrega, uma mensagem, contendo a entrega recém criada, é enviada ao caso de uso "Adicionar entrega" para que a solução registre-a à pessoa responsável por realizá-la.

São dois processos envolvidos: um que realiza a gestão de entregas e outro que realiza a gestão dos responsáveis (associação de entregas aos responsáveis e atualização de status das entregas pelos responsáveis).

Os componentes e os principais códigos-fonte envolvidos nesses casos de uso são:
* Entrega.API
![](doc/implementacao/entrega-api-criar.png)
* Entrega.Bloc
![](doc/implementacao/entrega-bloc-criar.PNG)
* BoaEntrega.Commons
![](doc/implementacao/boaentrega-commons-enviar.PNG)
* entregadb
![](doc/implementacao/entregadb-entrega-criada.PNG)
* Responsavel.Consumer
![](doc/implementacao/responsavel-consumer-message-handler.PNG)
* Responsavel.Bloc
![](doc/implementacao/responsavel-bloc-adicionar-entrega.PNG)
* responsaveldb
![](doc/implementacao/responsaveldb-entrega-adicionada.PNG)

Fazendo relação das principais implementações com os processos envolvidos, temos a figura logo abaixo:
![](doc/implementacao/compilado.png)

Os Requisitos não funcionais avaliados são:

RNF01) O sistema deve ser acessível nas plataformas web e móvel
* Critérios de aceitação:
1. Utilizar um formato leve de consumo de dados pelas plataformas web e móvel.
2. Utilizar um estilo arquitetural leve para comunicação das aplicações web e móvel com as funcionalidades e dados da solução.
3. Utilizar frameworks reconhecidos pelo mercado para o desenvolvimento das aplicações web e móvel
* Avaliação: o protótipo arquitetural utiliza JSON para consumo e troca de dados entre componentes e as funcionalidades são expostas para o frontend via API Gateway. O framework definido para a plataforma móvel é o flutter e para a plataforma web é o angular.

RNF02) O sistema deve possuir características de aplicação distribuída
* Critérios de aceitação:
1. Utilizar abordagem de microsserviços
2. Utilizar API Gateway
3. Utilizar Mensageria
* Avaliação: a solução possui API Gateway, Mensageria e APIs (microsserviços), de modo que as APIs se comunicam entre si por meio de mensagens e o frontend se comunica com o backend por meio do API Gateway.

RNF03) O sistema deve apresentar boa manutenibilidade
* Critério de aceitação:
1. Utilizar padrões e princípios arquiteturais
2. Ser desenvolvido em n camadas, de maneira que as camadas possuam responsabilidades bem definidas
* Avaliação: a solução foi desenvolvida utilizando interfaces e abstrações para permitir, dentre outras facilidades de manutenção, injeção de dependências. Além disso, alguns princípios, como o princípio aberto/fechado, está presente no protótipo arquitetural. Camadas estão bem definidas, a exemplo da camada responsável pela lógica de negócio e da camda armazenamento de dados.


## Mecanismos Arquiteturais
Esta seção apresenta uma visão geral dos mecanismos do protótipo arquitetural. Os mecanismos são baseados em três estados: (i) análise; (ii) design; e (iii) implementação.
os aspectos gerais da arquitetura do software são listados na análise. Os padrões tecnológicos de cada mecanismo da análise são listados no estado de design. As tecnologias/frameworks utilizados para cada design/análise são listados em implementação.
  
| Análise | Design | Implementação |
| ------ | ------ | ------ |
| Armazenamento | Banco de dados orientado a documentos | MongoDB |
| Backend | Web API | Dotnet core 5 |
| Mensageria | Stream de dados | Kafka |
| Gerenciamento de APIs | API Gateway | Ocelot |
| Documentação das APIs | OpenAPI | Swagger |
| Deploy | Container | Docker |
| Infraestrutura | Nuvem | Google Cloud Platform |
| Web | Single Page Application | Angular |
| Mobile | Abordagem Híbrida | Flutter |

## Diagrama de Classes
![](doc/diagramas-de-classes/entidades-entrega-bloc.png)
![](doc/diagramas-de-classes/entidades-responsavel-bloc.png)
![](doc/diagramas-de-classes/entidades-transporte-bloc.png)
![](doc/diagramas-de-classes/entrega-bloc.png)
![](doc/diagramas-de-classes/entrega-api.png)
![](doc/diagramas-de-classes/boaentrega-commons.png)
![](doc/diagramas-de-classes/entrega-consumer.png)
![](doc/diagramas-de-classes/responsavel-bloc.png)
![](doc/diagramas-de-classes/responsavel-api.png)
![](doc/diagramas-de-classes/responsavel-consumer.png)
![](doc/diagramas-de-classes/transporte-bloc.png)
![](doc/diagramas-de-classes/transporte-api.png)

## Endereço da POC
http://35.224.63.232:5200/swagger/index.html

## Vídeo de apresentação da POC
https://gitlab.com/t8828/boa-entrega/-/blob/main/doc/apresentacao-da-poc.mp4
